/*
 * Injamul
 */

window.onload = function () {
  function h(e) {
    return (
      e /
        parseInt(
          getComputedStyle(document.body).getPropertyValue("font-size")
        ) +
      "rem"
    );
  }
  function t(e, t) {
    var a,
      n,
      i,
      s,
      l,
      o = 20 * e.replace(/<(?:.|\n)*?>/gm, "").length + 500,
      r =
        ((a = e),
        (n = t),
        (i = document.createElement("div")),
        (s = document.createElement("span")),
        (l = document.createElement("span")),
        i.classList.add("bubble"),
        i.classList.add("is-loading"),
        i.classList.add("cornered"),
        i.classList.add("right" === n ? "right" : "left"),
        s.classList.add("message"),
        l.classList.add("loading"),
        (s.innerHTML = a),
        (l.innerHTML = "<b>•</b><b>•</b><b>•</b>"),
        i.appendChild(l),
        i.appendChild(s),
        (i.style.opacity = 0),
        { bubble: i, message: s, loading: l });
    p.appendChild(r.bubble), p.appendChild(document.createElement("br"));
    var b,
      m = (dimensions = {
        loading: { w: "4rem", h: "2.25rem" },
        bubble: {
          w: h((b = r).bubble.offsetWidth + 4),
          h: h(b.bubble.offsetHeight),
        },
        message: {
          w: h(b.message.offsetWidth + 4),
          h: h(b.message.offsetHeight),
        },
      });
    (r.bubble.style.width = "0rem"),
      (r.bubble.style.height = m.loading.h),
      (r.message.style.width = m.message.w),
      (r.message.style.height = m.message.h),
      (r.bubble.style.opacity = 1);
    var d = r.bubble.offsetTop + r.bubble.offsetHeight;
    d > p.offsetHeight && anime({ targets: p, scrollTop: d, duration: 750 });
    var g = anime({
        targets: r.bubble,
        width: ["0rem", m.loading.w],
        marginTop: ["2.5rem", 0],
        marginLeft: ["-2.5rem", 0],
        duration: 800,
        easing: "easeOutElastic",
      }),
      u = anime({
        targets: r.bubble,
        scale: [1.05, 0.95],
        duration: 1100,
        loop: !0,
        direction: "alternate",
        easing: "easeInOutQuad",
      }),
      c =
        (anime({
          targets: r.loading,
          translateX: ["-2rem", "0rem"],
          scale: [0.5, 1],
          duration: 400,
          delay: 25,
          easing: "easeOutElastic",
        }),
        anime({
          targets: r.bubble.querySelectorAll("b"),
          scale: [1, 1.25],
          opacity: [0.5, 1],
          duration: 300,
          loop: !0,
          direction: "alternate",
          delay: function (e) {
            return 100 * e + 50;
          },
        }));
    setTimeout(function () {
      u.pause(),
        c.restart({
          opacity: 0,
          scale: 0,
          loop: !1,
          direction: "forwards",
          update: function (e) {
            65 <= e.progress &&
              r.bubble.classList.contains("is-loading") &&
              (r.bubble.classList.remove("is-loading"),
              anime({ targets: r.message, opacity: [0, 1], duration: 300 }));
          },
        }),
        g.restart({
          scale: 1,
          width: [m.loading.w, m.bubble.w],
          height: [m.loading.h, m.bubble.h],
          marginTop: 0,
          marginLeft: 0,
          begin: function () {
            f < y.length && r.bubble.classList.remove("cornered");
          },
        });
    }, o - 50);
  }
  var e,
    a,
    p = document.querySelector(".messages"),
    f = 0,
    y = [
      "Hey there 👋",
      "I'm Injamul",
      "I code things on the web",
      'I\'m currently working as Full-stack Developer.<br> You can contact me at <a href="mailto:mrinjamul@gmail.com">mrinjamul@gmail.com</a>',
      '<a target="_blank" href="https://github.com/mrinjamul">github.com/mrinjamul</a><br/><a target="_blank" href="https://twitter.com/mrinjamul">twitter.com/mrinjamul</a>',
      ((e = new Date()),
      5 <= (a = e.getHours() + 0.01 * e.getMinutes()) && a < 19
        ? "Have a nice day"
        : 19 <= a && a < 22
        ? "Have a nice evening"
        : 22 <= a || a < 5
        ? "Have a good night"
        : void 0),
      "👀",
    ],
    n = function () {
      var e = y[f];
      e &&
        (t(e),
        ++f,
        setTimeout(
          n,
          20 * e.replace(/<(?:.|\n)*?>/gm, "").length + anime.random(900, 1200)
        ));
    };
  n();
};
